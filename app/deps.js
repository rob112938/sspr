import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import Typography from '@material-ui/core/Typography'

const store = createStore(reducer, composeWithDevTools(
  applyMiddleware(...middleware),
  // other store enhancers if any
));

<Typography variant='body1' component='h3' className={classes.error}>
{getError(2).reason}
</Typography>